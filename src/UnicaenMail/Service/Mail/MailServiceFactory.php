<?php

namespace UnicaenMail\Service\Mail;

use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use Symfony\Component\Mailer\Mailer;
use Symfony\Component\Mailer\Transport\Smtp\EsmtpTransport;

class MailServiceFactory {

    /**
     * @param ContainerInterface $container
     * @return MailService
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container) : MailService
    {
        $config = $container->get('Configuration')['unicaen-mail'];
        $transport = new EsmtpTransport(host: $config['transport_options']['host'], port: $config['transport_options']['port']);
        $mailer = new Mailer($transport);

        /**
         * @var EntityManager $entityManager
         */
        $entityManager = $container->get('doctrine.entitymanager.orm_default');

        $service = new MailService();
        $service->setMailer($mailer);
        $service->setConfig($config);
        $service->setEntityClass($config['mail_entity_class']);
        $service->setObjectManager($entityManager);
        return $service;
    }
}