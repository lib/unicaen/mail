<?php

namespace UnicaenMail\Service\Mail;

use DateInterval;
use DateTime;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;
use DoctrineModule\Persistence\ProvidesObjectManager;
use Exception;
use Laminas\Mvc\Controller\AbstractActionController;
use RuntimeException;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\Mailer;
use Symfony\Component\Mime\Email;
use UnicaenMail\Entity\Db\Mail;
use UnicaenMail\Exception\NotFoundConfigException;

class MailService
{
    use ProvidesObjectManager;

    const BODY_TEXT_TEMPLATE = <<<EOS

-----------------------------------------------------------------------
Ce mail a été redirigé.
Destinataires originaux :
To: %s
Cc: %s
Bcc: %s
Reply To: %s
EOS;
    const BODY_HTML_TEMPLATE = <<<EOS
<p>Ce mail a été redirigé.</p>
<p>
Destinataires originaux :<br />
To: %s<br />
Cc: %s<br />
Bcc: %s<br />
Reply To: %s
</p>
EOS;


    private ?Mailer $mailer;


    public function getMailer(): ?Mailer
    {
        return $this->mailer;
    }


    public function setMailer(?Mailer $mailer): void
    {
        $this->mailer = $mailer;
    }


    private ?string $entityClass = null;
    private array $config = [];


    /**
     * @param string $entityClass
     * @return MailService
     */
    public function setEntityClass(string $entityClass): MailService
    {
        $this->entityClass = $entityClass;
        return $this;
    }


    public function setConfig(array $config): void
    {
        $this->config = $config;
    }

    public function getConfig(): array
    {
        return $this->config;
    }

    public function createMailEntity(): object
    {
        $entity = new $this->entityClass();
        return $entity;
    }


    /** GESTION DES ENTITES *******************************************************************************************/

    public function create(Mail $mail): Mail
    {
        $this->objectManager->persist($mail);
        $this->objectManager->flush($mail);
        return $mail;
    }


    public function update(Mail $mail): Mail
    {
        $this->objectManager->flush($mail);
        return $mail;
    }


    public function delete(Mail $mail): Mail
    {
        $this->objectManager->remove($mail);
        $this->objectManager->flush($mail);
        return $mail;
    }


    /** REQUETAGE *****************************************************************************************************/

    public function createQueryBuilder(): QueryBuilder
    {
        $qb = $this->objectManager->getRepository($this->entityClass)->createQueryBuilder('mail');
        return $qb;
    }


    /**
     * @param string $champ
     * @param string $ordre
     * @return Mail[]
     */
    public function getMails(string $champ = 'dateEnvoi', string $ordre = 'DESC'): array
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('mail.dateEnvoi >= :july')->setParameter('july', DateTime::createFromFormat('d/m/Y', '01/07/2023'))
            ->orderBy('mail.' . $champ, $ordre);
        $result = $qb->getQuery()->getResult();
        return $result;
    }


    public function getMailsByMotClef(string $motClef, string $champ = 'dateEnvoi', string $ordre = 'DESC'): array
    {
        // todo ici un fix sal au probleme de MotClef1 === MotClef123 avec MotClef1 inclu dans MotClef123
        $qb = $this->createQueryBuilder()
            ->andWhere('mail.motsClefs like :motclefSHORT OR mail.motsClefs like :motclefLONG')
            ->setParameter('motclefSHORT', '%' . $motClef)
            ->setParameter('motclefLONG', '%' . $motClef . '||%')
            ->orderBy('mail.' . $champ, $ordre);
        $result = $qb->getQuery()->getResult();
        return $result;
    }


    public function getMail(?int $id): ?Mail
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('mail.id = :id')
            ->setParameter('id', $id);
        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs Mail partagent le même id [" . $id . "]", 0, $e);
        }
        return $result;
    }


    public function getRequestedMail(AbstractActionController $controller, string $param = 'mail'): ?Mail
    {
        $id = $controller->params()->fromRoute($param);
        $result = $this->getMail($id);
        return $result;
    }

    /** FACADE ********************************************************************************************************/

    /**
     * @throws NotFoundConfigException
     */
    public function fetchValueFromConfig(string $key, ?string $module = null, bool $optional = false)
    {
        $value = null;
        $config = $this->config['module'];
        if (isset($config[$module][$key])) $value = ($module) ? $config[$module][$key] : null;
        if ($value === null AND isset($config['default'][$key])) $value = $config['default'][$key];
        if ($value === null AND isset($config[$key])) $value = $config[$key];

        if (!$optional AND $value === null) {
            throw new NotFoundConfigException("Aucun valeur de trouver dans la configuration de UnicaenMail pour la clef [" . $key . "]");
        }
        return $value;
    }


    /** Transforme un mail Unicaen en Mail Symfony */
    public function transform(Mail $mail, ?string $module = null): Email
    {
        try {
            $fromEmail = $this->fetchValueFromConfig('from_email', $module);
            $fromName = $this->fetchValueFromConfig('from_name', $module);
            $redirect = $this->fetchValueFromConfig('redirect', $module, true);
            $replyTo = $this->fetchValueFromConfig('reply_to', $module, true);
        } catch (NotFoundConfigException $e) {
            throw new RuntimeException("Un problème est survenu lors de la récupération de valeurs de config", 0, $e);
        }

        $mailSymfony = new Email();
        $mailSymfony->from("'\"" . $fromName . "\" <" . $fromEmail . ">'");
        $adresseToArray = $redirect ? explode(',', $mail->getDestinatairesInitials()) : (explode(',', $mail->getDestinataires()));
        $mailSymfony->to(...$adresseToArray);
        if ($mail->getCopies() !== null AND $mail->getCopies() !== '') {
            $adresseCcArray = explode(',', $mail->getCopies());
            $mailSymfony->cc(...$adresseCcArray);
        }
        if ($mail->getReplyTo() !== null OR $replyTo !== null) {
            $mailSymfony->replyTo($mail->getReplyTo()??$replyTo);
        }
        $mailSymfony->subject($mail->getSujet());
        $mailSymfony->html($mail->getCorps());
        $mailSymfony->text(strip_tags($mail->getCorps()));

        return $mailSymfony;
    }

    public function sendMail($to, $subject, $texte, ?string $module = null, $attachement_path = null, $copie = null, $replyto = null): ?Mail
    {
        try {
            $doNotSend = $this->fetchValueFromConfig('do_not_send', $module);
            $redirect = $this->fetchValueFromConfig('redirect', $module);
            $redirectTo = (array)$this->fetchValueFromConfig('redirect_to', $module);
            $subjectPrefix = $this->fetchValueFromConfig('subject_prefix', $module);
            $replyto = ($replyto === null) ? $this->fetchValueFromConfig('reply_to', $module, true) : $replyto;
        } catch (NotFoundConfigException $e) {
            throw new RuntimeException("Un problème est survenu lors de la récupération de valeurs de config", 0, $e);
        }

        if ($to === null or $to === "" or $to === []) {
            return null;
        }

        /** @var Mail $mail */
        $mail = $this->createMailEntity();
        $mail->setDateEnvoi(new DateTime());
        $mail->setStatusEnvoi(Mail::PENDING);
        if ($redirect) {
            $mail->setDestinatairesInitials($to);
            $mail->setDestinataires(implode(',',$redirectTo));
        } else {
            $mail->setDestinataires($to);
        }
        if ($copie !== null and !empty($copie)) $mail->setCopies($copie);
        if ($replyto !== null and !empty($replyto)) $mail->setReplyTo($replyto);

        $sujet = "[" . $subjectPrefix . "] " . $subject;
        $mail->setSujet($sujet);
        $corps = "<p><i>Ce courrier électronique vous a été adressé <strong>automatiquement</strong> par l'application " . $subjectPrefix . ". </i></p>" . $texte;
        $mail->setCorps($corps);
        if ($attachement_path !== null) {
            $attachement_path = (is_string($attachement_path)) ? $attachement_path : implode("#<>#", $attachement_path);
            $mail->setAttachmentPaths($attachement_path);
        }
        $this->create($mail);

        if ($doNotSend !== true) {
            try {
                $mailSymfony = $this->transform($mail, $module);
                if ($redirect === true) {
                    $mailSymfony = $this->prepareMessageForRedirection($mailSymfony, $module);
                }
                $this->getMailer()->send($mailSymfony);
            } catch (TransportExceptionInterface $e) {
                throw new RuntimeException("Échec de l'envoi du message", 0, $e);
            }
            $mail->setStatusEnvoi(Mail::SUCCESS);
        } else {
            $mail->setStatusEnvoi(Mail::NOTSENT);
        }
        $this->update($mail);
        return $mail;
    }

    public function send(Email $mail, ?string $module = null): void
    {
        $doNotSend = $this->fetchValueFromConfig('do_not_send', $module);
        $redirect = $this->fetchValueFromConfig('redirect', $module);

        if ($doNotSend) {
            return;
        }

        if ($redirect) {
            $mail = $this->prepareMessageForRedirection($mail, $module);
        }

        try {
            $this->getMailer()->send($mail);
        } catch (TransportExceptionInterface $e) {
            throw new RuntimeException("Échec de l'envoi du message", 0, $e);
        }
    }


    protected function prepareMessageForRedirection(Email $mail, ?string $module = null): Email
    {
        // On crée un clone
        $redirMail = clone($mail);

        // Redirection effective
        $redirMail->getHeaders()->remove('To');  // Supprime tous les To
        $redirMail->getHeaders()->remove('Cc');  // Supprime tous les CC
        $redirMail->getHeaders()->remove('Bcc'); // Supprime tous les BCC
        $redirMail->getHeaders()->remove('Reply-To'); // Supprime tous les BCC

        $redirectTo = (array)$this->fetchValueFromConfig('redirect_to', $module);
        foreach ($redirectTo as $to) {
            $redirMail->addTo($to);
        }

        // Modification du sujet
        $redirMail->subject($mail->getSubject() . " {REDIR}");

        // Modification du corps
        $to = [];
        $cc = [];
        $bcc = [];
        $replyTo = [];
        foreach ($mail->getTo() as $addr) {
            $to[] = $addr->toString();
        }
        foreach ($mail->getCc() as $addr) {
            $cc[] = $addr->toString();
        }
        foreach ($mail->getBcc() as $addr) {
            $bcc[] = $addr->toString();
        }
        foreach ($mail->getReplyTo() as $addr) {
            $replyTo[] = $addr->toString();
        }
        $to = implode(", ", $to);
        $cc = implode(", ", $cc);
        $bcc = implode(", ", $bcc);
        $replyTo = implode(", ", $replyTo);

        if ($textBody = $mail->getTextBody()) {
            $textBody .= sprintf(self::BODY_TEXT_TEMPLATE, $to, $cc, $bcc, $replyTo);
            $redirMail->text($textBody);
        }
        if ($htmlBody = $mail->getHtmlBody()) {
            $htmlBody .= sprintf(self::BODY_HTML_TEMPLATE, $to, $cc, $bcc, $replyTo);
            $redirMail->html($htmlBody);
        }

        return $redirMail;
    }


    /** TODO : Le reenvoi ne tient pas compte du module ... */
    public function reenvoi(Mail $mail): Mail
    {
        $precedenteDateEnvoi = $mail->getDateEnvoi();
        $supplement = "<p><strong>Ce courrier électronique est une ré-expédition du courrier envoyé initialement le " . $precedenteDateEnvoi->format('d/m/Y à H:i') . "</strong></p>";

        $nMail = $this->sendMail($mail->getDestinataires(), $mail->getSujet(), ($supplement. $mail->getCorps()), null, $mail->getAttachmentPaths(), $mail->getReplyTo());
        $nMail->setMotsClefs(explode(Mail::MOTCLEF_SEPARATEUR, $mail->getMotsClefs()));
        $nMail->setCorps($mail->getCorps());
        $this->update($nMail);
        return $mail;

    }


    /** @return Mail[] */
    public function getMailsWithFiltre(array $filtres): array
    {
        $qb = $this->createQueryBuilder();

        if (isset($filtres['statut']) and $filtres['statut'] !== '') {
            $qb = $qb->andWhere('mail.statusEnvoi = :statut')->setParameter('statut', $filtres['statut']);
        }
        if (isset($filtres['date']) and $filtres['date'] !== '') {
            try {
                $date = (new DateTime())->sub(new DateInterval('P' . $filtres['date']));
            } catch (Exception $e) {
                throw new RuntimeException("Problème de calcul de la date buttoir avec [" . $filtres['date'] . "]", 0, $e);
            }
            $qb = $qb->andWhere('mail.dateEnvoi >= :date')->setParameter('date', $date);
        }
        $result = $qb->getQuery()->getResult();
        return $result;
    }
}