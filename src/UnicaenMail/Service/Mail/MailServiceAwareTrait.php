<?php

namespace UnicaenMail\Service\Mail;

trait MailServiceAwareTrait {

    private MailService  $mailService;

    public function getMailService(): MailService
    {
        return $this->mailService;
    }

    public function setMailService(MailService $mailService): void
    {
        $this->mailService = $mailService;
    }

}