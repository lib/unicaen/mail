<?php

namespace UnicaenMail\View\Helper;

use Laminas\View\Helper\AbstractHelper;
use Laminas\View\Helper\Partial;
use Laminas\View\Renderer\PhpRenderer;
use Laminas\View\Resolver\TemplatePathStack;
use UnicaenMail\Entity\Db\Mail;

class MailViewHelper extends AbstractHelper
{
    /**
     * @param Mail $mail
     * @param array $options
     * @return string|Partial
     */
    public function __invoke(Mail $mail, array $options = [])
    {
        /** @var PhpRenderer $view */
        $view = $this->getView();
        $view->resolver()->attach(new TemplatePathStack(['script_paths' => [__DIR__ . "/partial"]]));

        return $view->partial('mail', ['mail' => $mail, 'options' => $options]);
    }
}