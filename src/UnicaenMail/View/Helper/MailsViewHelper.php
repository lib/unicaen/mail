<?php

namespace UnicaenMail\View\Helper;

use Laminas\View\Renderer\PhpRenderer;
use UnicaenMail\Entity\Db\Mail;
use Laminas\View\Helper\AbstractHelper;
use Laminas\View\Helper\Partial;
use Laminas\View\Resolver\TemplatePathStack;

/**
 * Le tableau d'options peut recevoir un ensemble de booléen pour les droits afin de ne pas recalculer ou transmettre des valeurs différentes.
 * $options['droits']['afficher' => Boolean, 'reenvoyer' => Boolean, 'supprimer' => Boolean]
 */

class MailsViewHelper extends AbstractHelper
{
    /**
     * @param Mail[] $mails
     * @param array $options
     * @return string|Partial
     */
    public function __invoke(array $mails, array $options = [])
    {
        /** @var PhpRenderer $view */
        $view = $this->getView();
        $view->resolver()->attach(new TemplatePathStack(['script_paths' => [__DIR__ . "/partial"]]));

        return $view->partial('mails', ['mails' => $mails, 'options' => $options]);
    }
}