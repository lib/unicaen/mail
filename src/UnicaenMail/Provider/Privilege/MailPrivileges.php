<?php

namespace UnicaenMail\Provider\Privilege;

use UnicaenPrivilege\Provider\Privilege\Privileges;

/**
 * Liste des privilèges utilisables.
 */
class MailPrivileges extends Privileges
{
    const MAIL_INDEX                            = 'mail-mail_index';
    const MAIL_AFFICHER                         = 'mail-mail_afficher';
    const MAIL_TEST                             = 'mail-mail_test';
    const MAIL_REENVOI                          = 'mail-mail_reenvoi';
    const MAIL_SUPPRIMER                        = 'mail-mail_supprimer';
}