<?php

namespace UnicaenMail\Entity;

use Doctrine\Common\Collections\Collection;
use UnicaenMail\Entity\Db\Mail;
use UnicaenRenderer\Entity\Db\Template;

trait HasMailsTrait
{

    protected Collection $mails;

    /** @return  Mail[]  */
    public function getMails(): array
    {
        return $this->mails->toArray();
    }

    public function hasMail(Mail $mail): bool
    {
        return $this->mails->contains($mail);
    }

    public function addMail(Mail $mail): void
    {
        if (!$this->hasMail($mail)) { $this->mails->add($mail); }
    }

    public function removeMail(Mail $mail): void
    {
        $this->removeMail($mail);
    }

    public function hasMailWithTemplateCode(string|Template $template): bool
    {
        $code = ($template instanceof Template)?$template->generateTag():$template;

        /** @var Mail $mail */
        foreach ($this->mails as $mail) {
            $motsClefs = explode("||",$mail->getMotsClefs());
            if (in_array($code, $motsClefs)) return true;
        }
        return false;
    }

}