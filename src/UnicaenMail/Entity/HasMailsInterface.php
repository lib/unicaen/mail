<?php

namespace UnicaenMail\Entity;

use UnicaenMail\Entity\Db\Mail;
use UnicaenRenderer\Entity\Db\Template;

interface HasMailsInterface
{

    public function getMails(): array;
    public function hasMail(Mail $mail): bool;
    public function addMail(Mail $mail): void;
    public function removeMail(Mail $mail): void;
    public function hasMailWithTemplateCode(string|Template $template): bool;

}