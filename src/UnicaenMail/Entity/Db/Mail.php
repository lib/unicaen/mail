<?php

namespace UnicaenMail\Entity\Db;

use DateTime;

class Mail {

    const PENDING = 'PENDING';
    const SUCCESS = 'SUCCESS';
    const FAILED  = 'FAILED';
    const NOTSENT  = 'NOT SENT';
    const MOTCLEF_SEPARATEUR = '||';

    private ?int $id = null;
    private ?string $destinataires = null;
    private ?string $destinatairesInitials = null;
    private ?string $copies = null;
    private ?string $replyTo = null;
    private ?string $attachmentPaths = null;

    /** Contenu *******************************************************************************************************/

    private ?string $sujet = null;
    private ?string $corps = null;
    private ?string $motsClefs = null;

    /** STATUT ********************************************************************************************************/

    private ?DateTime $dateEnvoi = null;
    private ?string $statusEnvoi = null;
    private ?string $log = null;

    public function getId() : ?int
    {
        return $this->id;
    }

    public function getDateEnvoi() : ?DateTime
    {
        return $this->dateEnvoi;
    }

    public function setDateEnvoi(?DateTime $dateEnvoi) : void
    {
        $this->dateEnvoi = $dateEnvoi;
    }

    public function getStatusEnvoi() : ?string
    {
        return $this->statusEnvoi;
    }

    public function setStatusEnvoi(?string $statusEnvoi) : void
    {
        $this->statusEnvoi = $statusEnvoi;
    }

    public function getDestinataires() : ?string
    {
        return $this->destinataires;
    }

    public function setDestinataires(?string $destinataires) : void
    {
        $this->destinataires = $destinataires;
    }

    public function getDestinatairesInitials(): ?string
    {
        return $this->destinatairesInitials;
    }

    public function setDestinatairesInitials(?string $destinatairesInitials): void
    {
        $this->destinatairesInitials = $destinatairesInitials;
    }

    public function getCopies(): ?string
    {
        return $this->copies;
    }

    public function setCopies(?string $copies): void
    {
        $this->copies = $copies;
    }

    public function getReplyTo(): ?string
    {
        return $this->replyTo;
    }

    public function setReplyTo(?string $replyTo): void
    {
        $this->replyTo = $replyTo;
    }

    public function isRedirection() : bool
    {
        return $this->destinatairesInitials !== null;
    }

    public function getSujet() : ?string
    {
        return $this->sujet;
    }

    public function setSujet(?string $sujet) : void
    {
        $this->sujet = $sujet;
    }

    public function getCorps() : ?string
    {
        return $this->corps;
    }

    public function setCorps(?string $corps) : void
    {
        $this->corps = $corps;
    }

    public function getMotsClefs() : ?string
    {
        return $this->motsClefs;
    }

    public function setMotsClefs(array $motsClefs) : void
    {
        //Permet de s'assurer que les mots clefs ne sont pas vide
        $text = implode(Mail::MOTCLEF_SEPARATEUR, $motsClefs);
        $this->motsClefs = ($text && $text != '') ? $text : null;
    }

    public function getLog(): ?string
    {
        return $this->log;
    }

    public function setLog(?string $log): void
    {
        $this->log = $log;
    }

    public function getAttachmentPaths(): ?string
    {
        return $this->attachmentPaths;
    }

    public function setAttachmentPaths(?string $attachmentPaths): void
    {
        $this->attachmentPaths = $attachmentPaths;
    }

}