<?php

namespace UnicaenMail\Controller;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenMail\Service\Mail\MailService;

class MailControllerFactory
{

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): MailController
    {
        /**
         * @var MailService $mailService
         */
        $mailService = $container->get(MailService::class);

        $controller = new MailController();
        $controller->setMailService($mailService);
        return $controller;
    }
}