<?php

namespace UnicaenMail\Controller;

use Laminas\Http\Response;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;
use UnicaenMail\Service\Mail\MailServiceAwareTrait;

class MailController extends AbstractActionController
{
    use MailServiceAwareTrait;

    public function indexAction(): ViewModel
    {
        $filtre = $this->params()->fromQuery();
        if (!isset($filtre['date']) or $filtre['date'] === '') $filtre['date'] = '1W';

        $mails = $this->getMailService()->getMailsWithFiltre($filtre);

        return new ViewModel([
            'title' => 'Gestion des mails',
            'mails' => $mails,
            'params' => $filtre,
        ]);

    }

    public function afficherConfigurationAction(): ViewModel
    {
        $config = $this->getMailService()->getConfig();

        return new ViewModel([
            'title' => 'Configuration du module',
            'config' => $config,
        ]);
    }

    public function afficherAction(): ViewModel
    {
        $mail = $this->getMailService()->getRequestedMail($this);

        return new ViewModel([
            'title' => "Affichage du mail #" . $mail->getId(),
            'mail' => $mail,
        ]);
    }

    public function testAction(): array
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            if (!isset($data['mail-to']) || !filter_var($data['mail-to'], FILTER_VALIDATE_EMAIL)) {
                return ['title' => "Envoyer un mail de test", 'error' => "L'adresse mail saisie n'est pas valide."];
            }


            $mail = $this->getMailService()->sendMail(
                to: $data['mail-to'],
                subject: "Courrier électronique de test",
                texte: "Ceci est un mail de test. <br/> <hr/>Merci de ne pas en tenir compte.",
            );
            $mail->setMotsClefs(['TEST']);
            $this->getMailService()->update($mail);
        }
        return ['title' => "Envoyer un mail de test"];


    }

    public function reenvoiAction(): Response
    {
        $mail = $this->getMailService()->getRequestedMail($this);
        $this->getMailService()->reenvoi($mail);
        return $this->redirect()->toRoute('mail', [], [], true);
    }

    public function supprimerAction()
    {
        $mail = $this->getMailService()->getRequestedMail($this);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            if ($data["reponse"] === "oui") $this->getMailService()->delete($mail);
            exit();
        }

        $vm = new ViewModel();
        if ($mail !== null) {
            $vm->setTemplate('unicaen-mail/default/confirmation');
            $vm->setVariables([
                'title' => "Suppression du mail #" . $mail->getId(),
                'text' => "La suppression est définitive êtes-vous sûr&middot;e de vouloir continuer ?",
                'action' => $this->url()->fromRoute('mail/supprimer', ["mail" => $mail->getId()], [], true),
            ]);
        }
        return $vm;
    }
}