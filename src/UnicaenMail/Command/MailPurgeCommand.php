<?php

namespace UnicaenMail\Command;

use DateInterval;
use DateTime;
use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use UnicaenEtat\Entity\Db\EtatInstance;
use UnicaenEtat\Service\EtatInstance\EtatInstanceServiceAwareTrait;
use UnicaenMail\Entity\Db\Mail;
use UnicaenMail\Service\Mail\MailServiceAwareTrait;

/***
 * @desc Supprime toutes les instances d'état qui sont archivées depuis un certains temps
 * paramétre à définir dans config unicaen-etat > conservation-time sour la forme d'un DateInterval
 * Par défaut : 1 an
 * TODO : permettre dans la conf de spécifier les types et catégories d'états qui doivent être purgée
 * TODO : gérer des dates différents par état/types
 * TODO : rendre des types/catégories non purgeable
 */
class MailPurgeCommand extends Command
{
    use MailServiceAwareTrait;

    protected static $defaultName = 'mail:purge';


    protected ?DateInterval $conservationTime = null;
    public function setConservationTime(DateInterval $conservationTime): static
    {
        $this->conservationTime = $conservationTime;
        return $this;
    }
    public function getDateSuppression() : DateTime
    {
        $date = new DateTime();
        $conservationTime = ($this->conservationTime) ?? new DateInterval('P1Y');
        $date->sub($conservationTime);
        return $date;
    }

    protected function configure() : static
    {
        $this->setDescription("Supprime les instances de mails qui ont été envoyée depuis 'longtemps'");
        $this->addOption('time', '-t', InputOption::VALUE_OPTIONAL, "Intervale de temps que l'on souhaite conserver -- format : P1Y");
        return $this;
    }

    protected function initialize(InputInterface $input, OutputInterface $output): void
    {
        $io = new SymfonyStyle($input, $output);
        try{
            $time = $input->getOption('time');
            if(isset($time) && $time != ""){
                $this->conservationTime = new DateInterval($time);
            }
        }
        catch (Exception $e){
            $io->error($e->getMessage());
            exit(-1);
        }
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        try {
            $io->title("Suppression des instances de mails");

            $date = $this->getDateSuppression();
            $qb =$this->getMailService()->createQueryBuilder();
            $qb->andWhere("mail.dateEnvoi < :date");
            $qb->setParameter("date", $date);
            $mails = $qb->getQuery()->getResult();

            if(!empty($mails)) {
                $io->progressStart(sizeof($mails));
                /** @var Mail $mail */
                foreach ($mails as $mail) {
                    $this->getMailService()->delete($mail);
                    $io->progressAdvance();
                }
                $io->progressFinish();
            }
            else{
                $io->text("Aucun mail à supprimer");
            }
            $io->success("Suppression terminée");
        }
        catch (Exception $e){
                $io->error($e->getMessage());
                return self::FAILURE;
        }
        return self::SUCCESS;
    }

}