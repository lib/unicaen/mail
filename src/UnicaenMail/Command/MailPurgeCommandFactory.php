<?php

namespace UnicaenMail\Command;

use DateInterval;
use Exception;
use UnicaenMail\Service\Mail\MailService;
use Psr\Container\ContainerInterface;

class MailPurgeCommandFactory
{
    /**
     * @param ContainerInterface $container
     *
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     * @throws \Exception
     */
    public function __invoke(ContainerInterface $container): MailPurgeCommand
    {
        $command = new MailPurgeCommand();
        $command->setMailService($container->get(MailService::class));

        $config = $container->get('Configuration');
        if (isset($config['unicaen-mail']['conservation-time'])) {
            $conservationTime = $config['unicaen-mail']['conservation-time'];
            if(!$conservationTime instanceof DateInterval) {
                throw new Exception("Le paramètre de coniguration 'unicaen-mail > conservation-time' doit être un DateInterval");
            }
            $command->setConservationTime($conservationTime);
        }

        return $command;
    }
}