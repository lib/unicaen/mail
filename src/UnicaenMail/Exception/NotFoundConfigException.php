<?php

namespace UnicaenMail\Exception;

use Exception;

class NotFoundConfigException extends Exception {}