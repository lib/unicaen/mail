Module Unicaen Mail
=======================
------------------------

Description
-----------
Le module **unicaen/mail** est chargé de l'envoi et du stockage des courriers électroniques.

Le module fournit un menu dans Administration > Mail permettant d'accéder au listing des mails et des actions liées à ceux-ci.

Description du fonctionnement
============================

Les mails
----------

Les entités **Mail** sont stockés en base avec les informations suivantes :
* destinataire (redirection ou pas)
* date d'envoi
* status
* sujet/corps
* mots clefs

**MailService** est en chage de faire l'envoi et l'enregistrement en base.

Exemple d'utilisation sans utilisation d'un template :
```php
$mail = $this->getMailService()->sendMail('john.doe@aol.com', 'Reunion importante', 'Bonjour nous avons ...');
```

Exemple d'utilisation avec utilisation d'un template + entité liée : 
```php
$rendu = $this->getRenduService()->generateRenduByTemplateCode('MON TEMPLATE', ['VAR1' => $var1, 'VAR2' => $var2, ...]);
$mail = $this->getMailService()->sendMail('john.doe@aol.com', $rendu->getSujet(), $rendu->getCorps());
$mail->addMotsClefs(['MON_ENTITE', 'MON_TEMPLATE']);
$this->getMailService()->update($mail);
```

Aides de vue founies
--------------------

```php
'view_helpers' => [
    'invokables' => [
        'mail' => MailViewHelper::class,
        'mails' => MailsViewHelper::class,
    ],
],
```

`$this->mail($monMail)` : Affiche un mail généré avec la description à gauche et le texte généré à droite.
`$this->mails($monTableauDeMail)` : Affiche une table avec une ligne par mail et founi les actions associées (affiachage, réenvoi, suppression).

***Attention !!!*** l'aide `mails` peut recevoir un tableau permettant d'ajuster les droits des actions.
```php 
$options['droits'][
    'afficher' => Boolean, 
    'reenvoyer' => Boolean, 
    'supprimer' => Boolean
    ]
```

Purge des mails
---
La commande console `mail:purge` permet de supprimer les mails envoyées avant une date.

- La durée par défaut de conservertion est de 1 an, il s'agit d'un paramétre modifiable dans la configuration sous la forme de DateInterval :
```
    'unicaen-mail' => [
        'conservation-time' => new DateInterval('P2Y'),
    ],
```
1 option possibles :
- `--time=P2Y` *(ou `-t P2Y`)* pour spécifier la durée de conservations des mails (on ignore alors celle définie en conf).


Configuration
=============

La configuration locale à fournir est disponible dans [mail/config/unicaen-mail.local.php.dist]

```php
<?php
/**
 * Configuration locale du module UnicaenMail.
 */
return [
    'unicaen-mail' => [

        /**
         * Classe de entité
         **/
        'mail_entity_class' => Mail::class,
        
        /**
         * Options concernant l'envoi de mail par l'application
         */
        'transport_options' => [
            'host' => 'smtp.XXXX.fr',
            'port' => 25,
        ],
        /**
         * Adresses des redirection si do_not_send est à true
         */
        'redirect_to' => ['john-doe@mail.fr', ],
        'do_not_send' => true,
        'redirect' => true,
        
        /**
         * Configuration de l'expéditeur
         */
        'subject_prefix' => '[Mon App]',
        'from_name' => 'Mon application',
        'from_email' => 'ne-pas-repondre@mail.fr'
        
        /**
        * Durée de conservertions des mails
        */
        'conservation-time' => new DateInterval('P2Y'),
    ],
];
```

Tables pour les données du modules
==================================

**N.B.** Le script permettant de créer les tables est fourni dans le fichier [mail/SQL/001_tables.sql]

**unicaen_mail_mail** : table stockant les mails

| Column                 | Type | Obligatoire | Unique | Description                                                                                                                 |
|------------------------|---|---|---|-----------------------------------------------------------------------------------------------------------------------------|
| id                     | int | true | true | identifiant numerique du mail                                                                                               |
| date_envoi             | timestamp| true | false | date de l'envoi du mail                                                                                                     |
| status_envoi           | varchar(256) | true | false | statut "indicatif" de l'envoi : FAILED, SUCCESS, PENDING                                                                    |
| destinataires          | text | true | false | chaîne de caractères concaténant les adresses des destinataires                                                             |
| destinataires_initials | text | false | false | Null si le mail n'a pas été redirigé. Sinon chaînes de caractères concaténant les adresse des destinataires initiaux du mail |
| copies                 | text | false | false | Null si le mail n'a pas de destinataire en copie                                                                            |
| sujet                  | text | false | false | sujet du mail                                                                                                               |
| corps                  | text | false | false | corps du mail                                                                                                               |
| mots_clefs             | text | false | false | champ libre permettant d'associer des mots clefs aux mails (ie. id d'une entité, mots clés de recherche ...)                |
| log                    | text | false | false | Log du mail                                                                                                                 |

Privilèges associés au module
=============================

**N.B.** Le script permettant de créer les tables est fourni dans le fichier [mail/SQL/002_privileges.sql]

```php
const MAIL_INDEX                            = 'mail-mail_index';
const MAIL_AFFICHER                         = 'mail-mail_afficher';
const MAIL_TEST                             = 'mail-mail_test';
const MAIL_REENVOI                          = 'mail-mail_reenvoi';
const MAIL_SUPPRIMER                        = 'mail-mail_supprimer';
```

***Attention !!!*** Penser à donner les privilèges aux rôles adéquats.

Dépendances exterieurs
======================

Dépendance à **UnicaenPrivilège**
----------------------------------
1. Dans **vendor/unicaen/mail/config/merged/mail.config.php** : `UnicaenPrivilege\Guard\PrivilegeController` pour les gardes liées aux actions.
Peut être directement remplacer par l'equivalent fournit par `unicaen/auth`.

1. Dans **vendor/unicaen/mail/src/UnicaenMail/Provider/Privilege/MailPrivileges.php** : `UnicaenPrivilege\Provider\Privilege\Privileges` classe mère des privilèges du module. 
Peut être directement remplacer par l'equivalent fournit par `unicaen/auth`.

