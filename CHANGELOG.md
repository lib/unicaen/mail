7.2.2
-----
- Correction du bug lié à la réexpédition d'un mail
- Ajout d'une action pour controler la configuration du module

7.2.1
-----
- Bug Fixe pour les actions console

7.2.0
-----
- Ajout de la possibilité de préciser le reply-to (via la fonction d'envoi ou via la configuration clef "reply-to").
!! Attention !! ajout de la colonne reply_to dans unicaen_mail_mail 
```sql 
alter table unicaen_mail_mail add column reply_to text;
``` 

7.1.3
-----
- Mise en place de la commande mail:purge pour supprimer les mails envoyée avant une date

7.1.2
-----
- Correction pour caster la variable redirectTo en Array

7.1.1
-----
- Correction d'un paramète envoyé à tord aux mailer de symfony

7.1.0
-----
- Ajout d'une methode pour faire un envoi directement avec un objet Symfony Email
- Simplification de la méthode transformant un mail vers un entité

7.0.1
-----
- Correction de l'envoi vers des destinataires multiples

7.0.0
-----
- Utilisation de symfony/mail plutôt que de laminas/mail qui n'est plus maintenu

6.1.10
------

- [Fix] Correction de l'autoload par composer qui ne fonctionnait pas


6.1.1
-----
- Retrait de la dépendance à laminas-dependency-plugin

6.1.0
-----
- Compatibilité PHP8.2


6.0.6

* Possibilité de désactiver l'envoi de mail
* "do_not_send" est renommé en "redirect"
* un nouveau paramètre "do_not_send" n'envoie pas le mail