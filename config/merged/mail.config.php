<?php

use UnicaenMail\Controller\MailController;
use UnicaenMail\Controller\MailControllerFactory;
use UnicaenMail\Provider\Privilege\MailPrivileges;
use UnicaenMail\Service\Mail\MailService;
use UnicaenMail\Service\Mail\MailServiceFactory;
use UnicaenMail\View\Helper\MailsViewHelper;
use UnicaenMail\View\Helper\MailViewHelper;
use UnicaenPrivilege\Guard\PrivilegeController;
use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;
use UnicaenMail\Command\MailPurgeCommand;
use UnicaenMail\Command\MailPurgeCommandFactory;

return array(
    'bjyauthorize'    => [
        'guards' => [
            PrivilegeController::class => [
                [
                    'controller' => MailController::class,
                    'action'     => [
                        'index',
                    ],
                    'privileges' => [
                        MailPrivileges::MAIL_INDEX,
                    ],
                ],
                [
                    'controller' => MailController::class,
                    'action'     => [
                        'afficher',
                    ],
                    'privileges' => [
                        MailPrivileges::MAIL_AFFICHER,
                    ],
                ],
                [
                    'controller' => MailController::class,
                    'action'     => [
                        'test',
                    ],
                    'privileges' => [
                        MailPrivileges::MAIL_TEST,
                    ],
                ],
                [
                    'controller' => MailController::class,
                    'action'     => [
                        'reenvoi',
                    ],
                    'privileges' => [
                        MailPrivileges::MAIL_REENVOI,
                    ],
                ],
                [
                    'controller' => MailController::class,
                    'action'     => [
                        'afficher-configuration',
                        'supprimer',
                    ],
                    'privileges' => [
                        MailPrivileges::MAIL_SUPPRIMER,
                    ],
                ],
            ],
        ],
    ],



    'router' => [
        'routes' => [
            'mail' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/mail',
                    'defaults' => [
                        /** @see MailController::indexAction() */
                        'controller' => MailController::class,
                        'action' => 'index',
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'test' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => '/test',
                            'defaults' => [
                                /** @see MailController::testAction() */
                                'action' => 'test',
                            ],
                        ],
                    ],
                    'afficher' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => '/afficher/:mail',
                            'defaults' => [
                                /** @see MailController::afficherAction() */
                                'action' => 'afficher',
                            ],
                        ],
                    ],
                    'reenvoi' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => '/reenvoi/:mail',
                            'defaults' => [
                                /** @see MailController::reenvoiAction() */
                                'action' => 'reenvoi',
                            ],
                        ],
                    ],
                    'supprimer' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => '/supprimer/:mail',
                            'defaults' => [
                                /** @see MailController::supprimerAction() */
                                'action' => 'supprimer',
                            ],
                        ],
                    ],
                    'afficher-configuration' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => '/afficher-configuration',
                            'defaults' => [
                                /** @see MailController::afficherConfigurationAction() */
                                'action' => 'afficher-configuration',
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],

    'form_elements' => [
        'factories' => [],
    ],
    'hydrators' => [
        'factories' => [],
    ],
    'service_manager' => [
        'factories' => [
            MailService::class => MailServiceFactory::class,
            MailPurgeCommand::class => MailPurgeCommandFactory::class,
        ],

    ],
    'controllers' => [
        'factories' => [
            MailController::class => MailControllerFactory::class,
        ],
    ],
    'view_helpers' => [
        'invokables' => [
            'mail' => MailViewHelper::class,
            'mails' => MailsViewHelper::class,
        ],
    ],

    'laminas-cli' => [
        'commands' => [
            'mail:purge' => MailPurgeCommand::class,
        ],
    ],
);
